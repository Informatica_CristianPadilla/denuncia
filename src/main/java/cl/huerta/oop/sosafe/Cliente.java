/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.huerta.oop.sosafe;

import java.io.Serializable;

/**
 *
 * @author ppadilla
 */
public class Cliente implements Serializable {
    private String userName;
    private String Clave;

    public Cliente(String userName, String Clave) {
        this.userName = userName;
        this.Clave = Clave;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    public String getClave() {
        return Clave;
    }

 
 
    @Override
  public boolean equals(Object c) {
      if (c instanceof Cliente)
      { if(((Cliente) c).getUserName().equals(this.userName) && ((Cliente) c).getClave().equals(this.Clave))
      return true;}
              return false;
            }  
    
    
}
