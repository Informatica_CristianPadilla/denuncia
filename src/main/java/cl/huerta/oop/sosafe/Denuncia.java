/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.huerta.oop.sosafe;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author gohucan
 */
public final class Denuncia implements Comparable<Denuncia>,Serializable {

    private final double latitud;
    private final double longitud;
    private final String ubicacion;
    private String descripcion;
    private int prioridad;
    private TipoDenuncia tipo;
    private final Date fechaHora;

    public Denuncia(String ubicacion, String descripcion, int prioridad, TipoDenuncia tipo) {
        this.ubicacion = ubicacion;
        this.descripcion = descripcion;
        this.prioridad = prioridad;
        this.tipo = tipo;
        // pongo la fecha de ahora
        this.fechaHora = new Date();
        // aca deberia llamar a Google Maps y obtener la latitud y longitud
        this.latitud = 0.0;
        this.longitud = 0.0;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public TipoDenuncia getTipo() {
        return tipo;
    }

    public void setTipo(TipoDenuncia tipo) {
        this.tipo = tipo;
    }

    public Date getFechaHora() {
        return fechaHora;
    }
    
    @Override
    public int compareTo(Denuncia o) {
        return this.fechaHora.compareTo(o.getFechaHora());
    }

    @Override
    public String toString() {
        return String.format("Denuncia en %s a las %s", this.ubicacion, this.fechaHora);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Denuncia) {
            Denuncia copia = (Denuncia) obj;
            return this.fechaHora.compareTo(copia.getFechaHora()) == 0 && this.getTipo() == copia.getTipo();
        }
        if (obj instanceof TipoDenuncia) {
            return this.tipo == (TipoDenuncia)obj;
        }
        return false;
    }
    
    
    
    public static class ComparadorTipo implements Comparator<Denuncia> {

        @Override
        public int compare(Denuncia o1, Denuncia o2) {
            return o1.getTipo().compareTo(o2.getTipo());
        }
    
    }
    
    public static class ComparadorFecha implements Comparator<Denuncia> {

        @Override
        public int compare(Denuncia o1, Denuncia o2) {
            long l1 = o1.getFechaHora().getTime();
            long l2 = o2.getFechaHora().getTime();
            return (l1 < l2) ? -1 : (l1 == l2) ? 0 : 1;
        }
    
    }
    
    
}
